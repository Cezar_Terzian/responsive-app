import React from "react";
import "./App.css";
import { useGetCampaignsQuery } from "services/campagins";
import Loading from "components/Loading";
import Campaign from "components/Campaign";
// import file from "./response.json";
import "./components/index.css";

function App() {
  const { data, error, isLoading } = useGetCampaignsQuery();

  if (isLoading) {
    return <Loading isLoading={isLoading} />;
  }
  if (error) {
    return <h1>An error occured..! - {error.status}</h1>;
  }

  // const [data] = React.useState(file);

  return (
    <div className="App">
      {data.data.map((campaign) => (
        <Campaign key={campaign.id} campaign={campaign} />
      ))}
    </div>
  );
}

export default App;
