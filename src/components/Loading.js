import React from "react";
import { Spin, Space } from "antd";

const loadingStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "100vh",
};

const Loading = ({ isLoading }) => {
  if (!isLoading) return null;

  return (
    <Space style={loadingStyle}>
      <Spin size="large" />
    </Space>
  );
};

export default Loading;
