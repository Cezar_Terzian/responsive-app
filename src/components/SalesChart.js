import React from "react";
import { Progress } from "antd";

const SalesChart = ({ product_quantity, quantity_sold, ...props }) => {
  const percent = (quantity_sold / product_quantity) * 100;

  return (
    <Progress
      {...props}
      type="circle"
      style={{
        transform: "translateY(65px)",
        zIndex: 1,
        backgroundColor: "#fff",
        borderRadius: "100px",
      }}
      strokeColor="rgb(129, 26, 166)"
      percent={percent}
      format={() => (
        <div>
          <h5 style={{ color: "rgb(129, 26, 166)", margin: 0 }}>
            {quantity_sold}
          </h5>
          <h6>SOLD</h6>
          <h6 style={{ color: "grey", margin: 0 }}>OUT OF</h6>
          <h6 style={{ color: "grey" }}>{product_quantity}</h6>
        </div>
      )}
    />
  );
};

export default SalesChart;
