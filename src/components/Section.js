import React from "react";

const Section = ({ title, subTitle, imgURL, description, ...props }) => {
  return (
    <div {...props} style={{ textAlign: "center" }}>
      <img width="80%" src={imgURL} style={{ padding: "10px" }} alt={title} />
      <h1>{title}</h1>
      <h2>{subTitle}</h2>
      <p>{description}</p>
    </div>
  );
};

export default Section;
