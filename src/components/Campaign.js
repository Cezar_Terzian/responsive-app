import React from "react";
import SalesChart from "./SalesChart";
import { Card, Row, Col } from "antd";
import Section from "./Section";

const Campaign = ({ campaign }) => {
  const { product_id: product } = campaign;
  const { prize_id: prize } = campaign;

  return (
    <>
      <Row>
        <SalesChart
          quantity_sold={campaign.quantity_sold}
          product_quantity={campaign.product_quantity}
        />
      </Row>
      <Row>
        <Card
          style={{
            width: "75vw",
            marginBottom: "25px",
            borderRadius: "20px",
            boxShadow: "5px 5px 5px 5px lightgrey",
          }}
        >
          <Row wrap>
            <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 12 }}>
              <Section
                className="left-section"
                imgURL={product.image}
                title={product.name}
                description={product.description}
              />
            </Col>
            <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 12 }}>
              <Section
                imgURL={prize.image}
                title={prize.name}
                description={prize.description}
              />
            </Col>
          </Row>
        </Card>
      </Row>
    </>
  );
};

export default Campaign;
