// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

// Define a service using a base URL and expected endpoints
export const campaignsApi = createApi({
  reducerPath: "campaignsApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://wawinner.its.ae/dev/public/api/v1/",
  }),
  endpoints: (builder) => ({
    getCampaigns: builder.query({
      query: () => `front-end/campaign`,
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetCampaignsQuery } = campaignsApi;
